# -*- coding: utf-8 -*-

from odoo import models, fields, api

class LoanContract(models.Model):
    _name = 'loan.contract'

    name = fields.Many2one('res.partner')
    amount = fields.Float('Amount',default=1)
    company_id = fields.Many2one('res.company', 'Company', index=True, )
    confir_date = fields.Date('Date Confirmation')
    duration= fields.Integer('Loan duration', default= 1)
    end_date = fields.Date('Contract End')
